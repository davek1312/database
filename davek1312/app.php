<?php

return [

    'root_package_dir' => __DIR__.'/../',

    'registry' => [

        'Davek1312\Database\Registry',
        'Davek1312\Validation\Registry',
        'Davek1312\Translation\Registry',

    ],

];