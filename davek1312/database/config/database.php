<?php

return [

    'default' => davek1312_env('DATABASE_DEFAULT', 'default'),

    'migrations' => davek1312_env('DATABASE_MIGRATIONS', 'migrations'),

    'connections' => [

        'example_connection' => [

            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'database' => 'test',
            'username' =>  davek1312_env('DATABASE_USERNAME', ''),
            'password' =>  davek1312_env('DATABASE_PASSWORD', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => 'InnoDB',
        ],

    ],

];