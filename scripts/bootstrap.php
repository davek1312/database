<?php

use Davek1312\Database\Registry;

$registry = new Registry();
$manager = $registry->getManager();
$manager->setAsGlobal();
$manager->bootEloquent();