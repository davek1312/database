<?php

namespace Davek1312\Database;

use Davek1312\Database\Console\Commands\MigrationMake;
use Davek1312\Config\Config;
use Davek1312\Database\Console\Commands\MigrationMigrate;
use Davek1312\Database\Console\Commands\MigrationReset;
use Davek1312\Database\Console\Commands\MigrationRollback;
use Illuminate\Container\Container;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;

/**
 * Registers davek1312 components
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Registry extends \Davek1312\App\Registry {

    /**
     * Register the database configuration file
     *
     * @return void
     */
    protected function register() {
        $this->registerConfig('davek1312/database/config');
        $this->registerCommands([
            MigrationMake::class,
            MigrationMigrate::class,
            MigrationRollback::class,
            MigrationReset::class,
        ]);
    }

    /**
     * @return array
     */
    public function getDatabaseConnections() {
        $connections = $this->getDatabaseConfig('connections');
        $connections['default'] = $this->getDefaultConnection();
        return $connections;
    }

    /**
     * @return array
     */
    private function getDefaultConnection() {
        $defaultConnectionName = $this->getDefaultConnectionName();
        $defaultConnection = $this->getDatabaseConfig("connections.$defaultConnectionName");
        return $defaultConnection;
    }

    /**
     * @return string
     */
    private function getDefaultConnectionName() {
        return $this->getDatabaseConfig('default');
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    private function getDatabaseConfig($key) {
        $config = new Config();
        return $config->get("database.$key");
    }

    /**
     * @return Manager
     */
    public function getManager() {
        $manager = new Manager();
        foreach($this->getDatabaseConnections() as $name => $databaseConnection) {
            $manager->addConnection($databaseConnection, $name);
        }
        $manager->setEventDispatcher(new Dispatcher(new Container()));
        return $manager;
    }

    /**
     * @return ConnectionResolver
     */
    public function getConnectionResolver() {
        $connectionResolver = new ConnectionResolver($this->getManagerConnections());
        $connectionResolver->setDefaultConnection($this->getDefaultConnectionName());
        return $connectionResolver;
    }

    /**
     * Returns an array of \Illuminate\Database\Connection
     *
     * @return array
     */
    private function getManagerConnections() {
        $connections = [];
        $manager = $this->getManager();
        foreach($this->getDatabaseConnections() as $name => $databaseConnection) {
            $connections[$name] = $manager->getConnection($name);
        }
        return $connections;
    }

    /**
     * @return string
     */
    public function getMigrationsTable() {
        return $this->getDatabaseConfig('migrations');
    }
}