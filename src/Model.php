<?php

namespace Davek1312\Database;

use Davek1312\Database\Traits\ModelValidation;

/**
 * Additional functionality for \Illuminate\Database\Eloquent\Model
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Model extends \Illuminate\Database\Eloquent\Model {

    use ModelValidation;

    /**
     * @var array
     */
    protected $validationRules = [];
    /**
     * The message for any customer validation rules specific to the model.
     *
     * @var array
     */
    protected $customValidationMessages = [];
    /**
     * The locale for the validation messages. Will default to the app's locale.
     *
     * @var string
     */
    protected $validationLocale;
}