<?php

namespace Davek1312\Database\Testing;

use Davek1312\Console\Application;

/**
 * Rolls back the database migrations performed during the test
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
trait MakesDatabaseMigrations {

    /**
     * Run the migrations
     *
     * @return integer
     */
    public function setUpDatabaseMigrations() {
        return Application::call('migration:migrate');
    }

    /**
     * Rollback the migrations
     *
     * @return integer
     */
    public function tearDownDatabaseMigrations() {
        return Application::call('migration:rollback');
    }
}