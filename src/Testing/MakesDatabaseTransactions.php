<?php

namespace Davek1312\Database\Testing;

use Davek1312\Database\Registry;
use Illuminate\Database\Capsule\Manager;

/**
 * Rolls back all transactions performed during the test
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
trait MakesDatabaseTransactions {

    /**
     * @var array
     */
    private $connections = [];

    /**
     * Begin the database transactions
     *
     * @return void
     */
    public function setUpDatabaseTransactions() {
        $registry = new Registry();
        foreach($registry->getDatabaseConnections() as $name => $databaseConnection) {
            $connection = Manager::connection($name);
            $connection->beginTransaction();
            $this->connections[] = $connection;
        }
    }

    /**
     * Rollback the database transactions
     *
     * @return void
     */
    public function tearDownDatabaseTransactions() {
        foreach($this->connections as $connection) {
            $connection->rollBack();
        }
    }
}