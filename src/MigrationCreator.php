<?php

namespace Davek1312\Database;

use Davek1312\Database\Console\Commands\MigrationMake;
use Davek1312\Config\Config;
use Davek1312\Database\Console\Commands\MigrationMigrate;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Capsule\Manager;

/**
 * Overrides stubPath
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MigrationCreator extends \Illuminate\Database\Migrations\MigrationCreator {

    /**
     * Get the path to the stubs.
     *
     * @return string
     */
    public function stubPath() {
        return __DIR__.'/../stubs';
    }
}