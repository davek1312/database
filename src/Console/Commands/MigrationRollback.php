<?php

namespace Davek1312\Database\Console\Commands;

/**
 * Command to rollback the database
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MigrationRollback extends BaseMigrationCommand {

    protected $signature = 'migration:rollback';
    protected $description = 'Rolls back the database to the previous migration batch';

    protected function process() {
        parent::process();
        $this->rollback();
    }

    /**
     * @return void
     */
    private function rollback() {
        $migrationsRolledBack = $this->migrator->rollback($this->app->getMigrations());
        if(empty($migrationsRolledBack)) {
            $this->outputInfo('No migrations to rollback.');
        }
        else {
            foreach($migrationsRolledBack as $migration) {
                $this->outputInfo("$migration rolled back.");
            }
        }
    }
}