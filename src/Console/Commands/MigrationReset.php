<?php

namespace Davek1312\Database\Console\Commands;

/**
 * Command to reset the database
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MigrationReset extends BaseMigrationCommand {

    protected $signature = 'migration:reset';
    protected $description = 'Rollbacks all migrations and runs them all again';

    protected function process() {
        parent::process();
        $this->reset();
    }

    /**
     * @return void
     */
    private function reset() {
        $migrationsReset = $this->migrator->reset($this->app->getMigrations());
        if(empty($migrationsReset)) {
            $this->outputInfo('No migrations to reset.');
        }
        else {
            foreach($migrationsReset as $migration) {
                $this->outputInfo("$migration reset.");
            }
        }
    }
}