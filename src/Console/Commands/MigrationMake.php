<?php

namespace Davek1312\Database\Console\Commands;

use Davek1312\Console\Command;
use Davek1312\Database\MigrationCreator;
use Illuminate\Filesystem\Filesystem;

/**
 * Command to create migration files
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MigrationMake extends Command {

    protected $signature = 'migration:make {name : The name of the migration file} {--path=davek1312/database/migrations : The path the migration file will be created at} {--table= : The database table name} {--create : Defines if the migration is used to create a table}';
    protected $description = 'Create a migration class';

    protected function process() {
        $name = $this->getArgument('name');
        $migrationCreator = new MigrationCreator(new Filesystem());
        $migrationCreator->create($name, $this->getOption('path'), $this->getOption('table'), $this->getOption('create'));
        $this->outputInfo("Created migration $name");
    }
}