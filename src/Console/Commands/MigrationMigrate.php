<?php

namespace Davek1312\Database\Console\Commands;

/**
 * Command to manage registered migrations
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class MigrationMigrate extends BaseMigrationCommand {

    protected $signature = 'migration:migrate';
    protected $description = 'Perform the migrations';

    protected function process() {
        parent::process();
        $this->runMigrations();
    }

    /**
     * @return void
     */
    private function runMigrations() {
        $migrationsRan = $this->migrator->run($this->app->getMigrations());
        if(empty($migrationsRan)) {
            $this->outputInfo('No migrations ran.');
        }
        else {
            foreach($migrationsRan as $migration) {
                $this->outputInfo("$migration run.");
            }
        }
    }
}