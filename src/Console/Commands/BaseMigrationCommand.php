<?php

namespace Davek1312\Database\Console\Commands;

use Davek1312\App\App;
use Davek1312\Console\Command;
use Davek1312\Database\Registry;
use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Filesystem\Filesystem;

/**
 * Migration manager
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class BaseMigrationCommand extends Command {

    /**
     * @var App;
     */
    protected $app;

    /**
     * @var Migrator;
     */
    protected $migrator;

    /**
     * @var Registry
     */
    private $registry;

    public function __construct() {
        parent::__construct();
        $this->app = new App();
        $this->registry = new Registry();
        $this->migrator = $this->getMigrator();
    }

    protected function process() {
        $this->createMigrationTableIfNotExists();
    }

    /**
     * @return Migrator
     */
    protected function getMigrator() {
        return new Migrator($this->getDatabaseMigrationRepository(), $this->registry->getConnectionResolver(), new Filesystem());
    }

    /**
     * @return DatabaseMigrationRepository
     */
    private function getDatabaseMigrationRepository() {
        return new DatabaseMigrationRepository($this->registry->getConnectionResolver(), $this->registry->getMigrationsTable());
    }

    /**
     * @return App
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @param App $app
     */
    public function setApp($app) {
        $this->app = $app;
    }

    /**
     * @return Registry
     */
    public function getRegistry() {
        return $this->registry;
    }

    /**
     * @param Registry $registry
     */
    public function setRegistry($registry) {
        $this->registry = $registry;
    }

    /**
     * @return void
     */
    private function createMigrationTableIfNotExists() {
        if(!$this->migrator->repositoryExists()) {
            $this->outputComment('Migration table does not exist. Creating Migration table.');
            $this->migrator->getRepository()->createRepository();
            $this->outputInfo('Migration table created.');
        }
    }
}