<?php

namespace Davek1312\Database\Traits;

use Davek1312\Validation\Validator;

/**
 * Trait to validate the model attributes
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
trait ModelValidation {

    /**
     * Listen for save event
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();
        static::saving(function($model)  {
            return $model->isValid();
        });
    }

    /**
     * @return Validator;
     */
    public function getValidator() {
        return new Validator($this->attributes, $this->getValidationRules(), $this->getCustomValidationMessages(), $this->getValidationLocale());
    }

    /**
     * @return boolean
     */
    public function isValid() {
        return $this->getValidator()->passes();
    }

    /**
     * @return \Illuminate\Support\MessageBag
     */
    public function getValidationMessages() {
        return $this->getValidator()->getMessageBag();
    }

    /**
     * @return array
     */
    public function getValidationRules() {
        return $this->validationRules;
    }

    /**
     * @param array $validationRules
     */
    public function setValidationRules($validationRules) {
        $this->validationRules = $validationRules;
    }

    /**
     * @return array
     */
    public function getCustomValidationMessages() {
        return $this->customValidationMessages;
    }

    /**
     * @param array $customValidationMessages
     */
    public function setCustomValidationMessages($customValidationMessages) {
        $this->customValidationMessages = $customValidationMessages;
    }

    /**
     * @return string
     */
    public function getValidationLocale() {
        return $this->validationLocale;
    }

    /**
     * @param string $validationLocale
     */
    public function setValidationLocale($validationLocale) {
        $this->validationLocale = $validationLocale;
    }
}