# davek1312/database

Bootstraps the illuminate/database package.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/database),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/database
```

# Configuration
Copy `vendor\davek1312\database\davek1312` into your application's root directory.

## Database Connections
Add your database connections into the `connections` array in `davek1312\database\config\database.php` in your application's root directory.
```php
<?php 
return [
    
    // Default connection that will be used
    'default' => davek1312_env('DATABASE_DEFAULT', 'default'),
    
    // Migrations table name
    'migrations' => davek1312_env('DATABASE_MIGRATIONS', 'migrations'),
    
    'connections' => [
        
        'example_connection' => [
            'driver' => 'mysql',
            'host' => davek1312_env('EXAMPLE_CONNECTION_HOST', 'default_database_host'),
            'port' => davek1312_env('EXAMPLE_CONNECTION_PORT', 'default_database_port'),
            'database' => davek1312_env('EXAMPLE_CONNECTION_DATABASE', 'default_database_name'),
            'username' => davek1312_env('EXAMPLE_CONNECTION_USERNAME', 'default_database_username'),
            'password' => davek1312_env('EXAMPLE_CONNECTION_PASSWORD', 'default_database_password'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
        ],
        
    ], 
    
];
```

# Eloquent Usage
You can view the Eloquent documentation on [Laravel](https://laravel.com/docs/eloquent).

# Migrations 

## Creating a migration
```bash
vendor\bin\davek1312-console migration:make name --path= --table= --create
```

## Register Migrations
To register your migrations view the [davek1312\app](https://packagist.org/packages/davek1312/app) documentation.

## Running The Migrations
```bash
vendor\bin\davek1312-console migration:migrate
```

## Rolling Back The Migrations
```bash
vendor\bin\davek1312-console migration:rollback
```

## Resetting The Database
```bash
vendor\bin\davek1312-console migration:reset
```

#Validation
You can validate your Eloquent models when saving/updating the model. You can either extend our version of Eloquent's model or use our model validation trait.

## Extend Model
```php
<?php

namespace YourNamespace;

class YourModel extends \Davek1312\Database\Model {
}
```

## Use Validation Trait
```php
<?php

namespace YourNamespace;

class YourModel extends \Illuminate\Database\Eloquent\Model {
    
    use \Davek1312\Database\Traits\ModelValidation;
}
```

## Model Validation Configuration
You can view the [valdation](https://packagist.org/packages/davek1312/validation) documentation to see what rules are available and to implement custom messages.
```php
<?php

namespace YourNamespace;

class YourModel extends \Davek1312\Database\Model {
    /**
     * @var array
     */
    protected $fillable = ['name'];
    /**
     * @var array
     */
    protected $validationRules = ['name' => 'required'];
    /**
     * The message for any customer validation rules specific to the model. 
     * 
     * @var array
     */
    protected $customValidationMessages = [];
    /**
     * The locale for the validation messages. Will default to the app's locale.
     * 
     * @var string
     */
    protected $validationLocale;
}
```

## Validation Methods

```php
<?php

$model = new YourModel($data);
// Will only save if isValid() returns true. Applies to saving/updating/creating events.
$model->save();

// Checks if the model passes validation
$model->isValid();

//Returns all the validation messages for failed rules
$model->getValidationMessages();
```
