<?php

namespace Davek1312\Database\Tests\Traits;

use Davek1312\Database\Tests\Console\Commands\MigrationMigrateTest;
use Davek1312\Database\Tests\Mock\MockModel;

class ModelValidationTest extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        parent::setUp();
        MigrationMigrateTest::deleteMockTables();
    }

    public function tearDown() {
        parent::tearDown();
        MigrationMigrateTest::deleteMockTables();
    }

    public function testBoot() {
        MigrationMigrateTest::createMockTables();
        $validModel = new MockModel([
            'name' => 'name'
        ]);
        $this->assertValidModelSaved($validModel);

        $inValidModel = new MockModel();
        $this->assertValidInModelNotSaved($inValidModel);
    }

    private function assertValidModelSaved($validModel) {
        $this->assertTrue($validModel->save());
        $this->assertEquals('', $validModel->getValidationMessages()->first());
    }

    private function assertValidInModelNotSaved($inValidModel) {
        $this->assertFalse($inValidModel->save());
        $this->assertEquals('The name field is required.', $inValidModel->getValidationMessages()->first());
    }

    public function testGetValidator(){
        $model = new MockModel();
        $validator = $model->getValidator();
        $this->assertEquals([
            'name' => ['required'],
        ], $validator->getRules());
    }

    public function testIsValid() {
        $model = new MockModel();
        $validator = $model->getValidator();
        $this->assertEquals($validator->passes(), $model->isValid());
    }

    public function testGetValidationMessages() {
        $model = new MockModel();
        $validator = $model->getValidator();
        $this->assertEquals($validator->getMessageBag(), $model->getValidationMessages());
    }
}