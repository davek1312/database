<?php

namespace Davek1312\Database\Tests\Testing;

use Davek1312\Database\Testing\MakesDatabaseTransactions;
use Davek1312\Database\Tests\Console\Commands\MigrationMigrateTest;
use Davek1312\Database\Tests\Mock\MockModel;

class MakesDatabaseTransactionsTest extends \PHPUnit_Framework_TestCase {

    use MakesDatabaseTransactions;

    public function setUp() {
        parent::setUp();
        MigrationMigrateTest::deleteMockTables();
    }

    public function tearDown() {
        parent::tearDown();
        MigrationMigrateTest::deleteMockTables();
    }

    public function testSetUpAndTearDownDatabaseTransactions() {
        MigrationMigrateTest::createMockTables();
        $this->setUpDatabaseTransactions();
        $model = MockModel::create([
            'name' => 'test',
        ]);
        $this->assertNotNull(MockModel::find($model->id));
        $this->tearDownDatabaseTransactions();
        $this->assertNull(MockModel::find($model->id));
    }
}