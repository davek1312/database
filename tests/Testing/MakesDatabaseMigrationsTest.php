<?php

namespace Davek1312\Database\Tests\Testing;

use Davek1312\Console\Application;
use Davek1312\Database\Testing\MakesDatabaseMigrations;

class MakesDatabaseMigrationsTest extends \PHPUnit_Framework_TestCase {

    use MakesDatabaseMigrations;

    public function tetsSetUpDatabaseMigrations() {
       $this->assertEquals(Application::call('migration:migrate'), $this->setUpDatabaseMigrations());
    }

    public function testTearDownDatabaseMigrations() {
        $this->assertEquals(Application::call('migration:rollback'), $this->tearDownDatabaseMigrations());
    }
}