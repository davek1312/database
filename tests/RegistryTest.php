<?php

namespace Davek1312\Database\Tests;

use Davek1312\Database\Console\Commands\MigrationMake;
use Davek1312\Database\Console\Commands\MigrationMigrate;
use Davek1312\Database\Console\Commands\MigrationReset;
use Davek1312\Database\Console\Commands\MigrationRollback;
use Davek1312\Database\Registry;

class RegistryTest extends \PHPUnit_Framework_TestCase {

    private $registry;

    public function setUp() {
        parent::setUp();
        $this->registry = new Registry();
    }

    public function testConfigRegistered() {
        $this->assertEquals([
            'davek1312/database/config',
        ], $this->registry->getConfig());
    }

    public function testCommandsRegistered() {
        $this->assertEquals([
            MigrationMake::class,
            MigrationMigrate::class,
            MigrationRollback::class,
            MigrationReset::class,
        ], $this->registry->getCommands());
    }

    public function testGetDatabaseConnections() {
        $connection = [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'database' => 'test',
            'username' => davek1312_env('DATABASE_USERNAME'),
            'password' => davek1312_env('DATABASE_PASSWORD'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => 'InnoDB',
        ];
        $this->assertEquals([
            'example_connection' => $connection,
            'default' => $connection,
        ], $this->registry->getDatabaseConnections());
    }

    public function testGetManager() {
        $manager = $this->registry->getManager();
        $this->assertNotNull($manager->getConnection('example_connection'));
    }

    public function testGetConnectionResolver() {
        $connectionResolver = $this->registry->getConnectionResolver();
        $this->assertTrue($connectionResolver->hasConnection('example_connection'));
        $this->assertEquals('example_connection', $connectionResolver->getDefaultConnection());
    }

    public function testgetMigrationsTable() {
        $this->assertEquals('migrations', $this->registry->getMigrationsTable());
    }
}