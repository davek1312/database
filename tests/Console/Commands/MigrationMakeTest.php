<?php

namespace Davek1312\Database\Tests\Console\Commands;

use Davek1312\Console\Tests\CommandTest;
use Davek1312\Database\Console\Commands\MigrationMake;
use Illuminate\Filesystem\Filesystem;

class MigrationMakeTest extends \PHPUnit_Framework_TestCase {

    private $fileSystem;
    private $tempMockMigrationsDirectory;

    public function setUp() {
        parent::setUp();
        $this->fileSystem = new Filesystem();
        $this->tempMockMigrationsDirectory = 'tests/Mock/temp-migrations';
    }

    public function tearDown() {
        parent::tearDown();
        foreach($this->getMigrationFiles() as $migrationFile) {
            $this->fileSystem->delete($migrationFile);
        }
    }

    public function testMigrationIsMade() {
        $this->assertEmpty($this->getMigrationFiles());
        $this->getMigrationMakeOutput();

        $migrationFiles = $this->getMigrationFiles();
        $this->assertNotEmpty($migrationFiles);
        $migrationFile = $migrationFiles[0];
        $this->assertContains('test', $migrationFile);
    }

    private function getMigrationFiles() {
        return $this->fileSystem->files($this->tempMockMigrationsDirectory);
    }

    private function getMigrationMakeOutput() {
        $command = CommandTest::getCommand(MigrationMake::class, 'migration:make');
        return CommandTest::getCommandOutput($command, [
            'name' => 'test',
            '--path' => $this->tempMockMigrationsDirectory,
            '--table' => 'test',
            '--create' => true,
        ]);
    }
}