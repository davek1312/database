<?php

namespace Davek1312\Database\Tests\Console\Commands;

use Davek1312\App\App;
use Davek1312\Database\Console\Commands\MigrationMigrate;
use Illuminate\Database\Capsule\Manager;
use Davek1312\Console\Tests\CommandTest;

class MigrationMigrateTest extends \PHPUnit_Framework_TestCase {

    const MOCK_MIGRATIONS_DIRECTORY = 'tests/Mock/migrations';

    public function setUp() {
        parent::setUp();
        static::deleteMockTables();
    }

    public function tearDown() {
        parent::tearDown();
        static::deleteMockTables();
    }

    public function testMigrationMigrated() {
        $output = static::getMigrationMigrateOutput();
        $this->assertMigrationTableCreated($output);
        $this->assertMigrationRan($output);

        $output = static::getMigrationMigrateOutput();
        $this->assertMigrationNotRunAgain($output);
    }

    private function assertMigrationTableCreated($output) {
        $this->assertTrue(Manager::schema()->hasTable('migrations'));
        $this->assertContains('Migration table does not exist. Creating Migration table.', $output);
        $this->assertContains('Migration table created.', $output);
    }

    private function assertMigrationRan($output) {
        $this->assertTrue(Manager::schema()->hasTable('mock'));
        $this->assertContains('tests/Mock/migrations/2017_03_26_164534_mock.php run.', $output);
        $migration = Manager::table('migrations')->where('migration', '2017_03_26_164534_mock')->first();
        $this->assertNotNull($migration);
    }

    private function assertMigrationNotRunAgain($output) {
        $this->assertContains('No migrations ran.', $output);
    }

    public static function getMigrationMigrateOutput() {
        return static::getMigrationCommandOutput(MigrationMigrate::class, 'migration:migrate');
    }

    public static function getMigrationCommandOutput($commandClass, $commandName) {
        $app = new App();
        $app->setMigrations([static::MOCK_MIGRATIONS_DIRECTORY]);
        $command = CommandTest::getCommand($commandClass, $commandName);
        $command->setApp($app);
        return CommandTest::getCommandOutput($command);
    }

    public static function createMockTables() {
        static::getMigrationMigrateOutput();
    }

    public static function deleteMockTables() {
        Manager::schema()->dropIfExists('migrations');
        Manager::schema()->dropIfExists('mock');
    }
}