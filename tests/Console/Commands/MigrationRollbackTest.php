<?php

namespace Davek1312\Database\Tests\Console\Commands;

use Davek1312\Database\Console\Commands\MigrationRollback;
use Illuminate\Database\Capsule\Manager;

class MigrationRollbackTest extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        parent::setUp();
        MigrationMigrateTest::deleteMockTables();
    }

    public function tearDown() {
        parent::tearDown();
        MigrationMigrateTest::deleteMockTables();
    }

    public function testMigrationsRolledBack() {
        $output = $this->getMigrationRollbackOutput();
        $this->assertNothingToRollback($output);

        MigrationMigrateTest::createMockTables();
        $output = $this->getMigrationRollbackOutput();
        $this->assertMigrationsRolledBack($output);
    }

    private function assertNothingToRollback($output) {
        $this->assertContains('No migrations to rollback.', $output);
    }

    private function assertMigrationsRolledBack($output) {
        $this->assertFalse(Manager::schema()->hasTable('mock'));
        $this->assertContains('tests/Mock/migrations/2017_03_26_164534_mock.php rolled back.', $output);
        $migration = Manager::table('migrations')->where('migration', '2017_03_26_164534_mock')->first();
        $this->assertNull($migration);
    }

    private function getMigrationRollbackOutput() {
        return MigrationMigrateTest::getMigrationCommandOutput(MigrationRollback::class, 'migration:rollback');
    }
}