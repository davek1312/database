<?php

namespace Davek1312\Database\Tests\Console\Commands;

use Davek1312\Database\Console\Commands\MigrationReset;
use Illuminate\Database\Capsule\Manager;

class MigrationResetTest extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        parent::setUp();
        MigrationMigrateTest::deleteMockTables();
    }

    public function tearDown() {
        parent::tearDown();
        MigrationMigrateTest::deleteMockTables();
    }

    public function testMigrationsRolledBack() {
        $output = $this->getMigrationResetOutput();
        $this->assertNothingToReset($output);

        MigrationMigrateTest::createMockTables();
        $output = $this->getMigrationResetOutput();
        $this->assertMigrationsReset($output);
    }

    private function assertNothingToReset($output) {
        $this->assertContains('No migrations to reset.', $output);
    }

    private function assertMigrationsReset($output) {
        $this->assertFalse(Manager::schema()->hasTable('mock'));
        $this->assertContains('tests/Mock/migrations/2017_03_26_164534_mock.php reset.', $output);
        $migration = Manager::table('migrations')->where('migration', '2017_03_26_164534_mock')->first();
        $this->assertNull($migration);
    }

    private function getMigrationResetOutput() {
        return MigrationMigrateTest::getMigrationCommandOutput(MigrationReset::class, 'migration:reset');
    }
}