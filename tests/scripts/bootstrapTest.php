<?php

namespace Davek1312\Database\Tests\Scripts;

use Davek1312\Database\Tests\Console\Commands\MigrationMigrateTest;
use Illuminate\Database\Capsule\Manager;

class BootstrapTest extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        parent::setUp();
        MigrationMigrateTest::deleteMockTables();
    }

    public function tearDown() {
        parent::tearDown();
        MigrationMigrateTest::deleteMockTables();
    }

    public function testConnectionsBootstrapped() {
        MigrationMigrateTest::createMockTables();
        $this->assertTrue(Manager::schema()->hasTable('mock'));
    }
}