<?php

namespace Davek1312\Database\Tests\Mock;

use Davek1312\Database\Model;

class MockModel extends Model {

    protected $table = 'mock';
    protected $fillable = ['name'];
    protected $validationRules = ['name' => 'required'];
}