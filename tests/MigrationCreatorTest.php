<?php

namespace Davek1312\Database\Tests;

use Davek1312\Database\MigrationCreator;
use Illuminate\Filesystem\Filesystem;

class MigrationCreatorTest extends \PHPUnit_Framework_TestCase {

    private $migrationCreator;

    public function setUp() {
        parent::setUp();
        $this->migrationCreator = new MigrationCreator(new Filesystem());
    }

    public function testStubPath() {
        $this->assertContains('src/../stubs', $this->migrationCreator->stubPath());
    }
}